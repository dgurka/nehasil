<?php

// define constants, there are seperate sets for local and live

define("WEBSITE_TITLE", "Nehasil Memorial Park");

define("BASE_DIR", dirname(__FILE__) . "/");
define("TEMPLATE_DIR", BASE_DIR . "templates/");
define("TEMPLATE_PACKAGE", "nehasil");
define("ADMIN_TEMPLATE_PACKAGE", "admin"); // use this in the 
define("DISPLAY_ERRORS", true);
define("ADMIN_EMAIL", "rstevenson@michiganpolicechiefs.org");
define("NO_REPLY_EMAIL", "no-reply@enablepoint.com");


if($_SERVER["HTTP_HOST"] == "localhost")
{
	// localhost server details
	define("URI_BASE", "\/nehasil2\/");
	
	define("REQUEST_URI", preg_replace("/^" . URI_BASE . "/", "", $_SERVER["REQUEST_URI"]));

	define("URL_ROOT", "/nehasil2/");

	define("DB_SERVER", "127.0.0.1");
	define("DB_USER", "root");
	define("DB_PASSWORD", "");
	define("DB_DATABASE", "nehasil2");
}
else
{
	// "live" server details
	define("URI_BASE", "/");
	
	define("REQUEST_URI", ltrim($_SERVER["REQUEST_URI"], "/"));

	define("URL_ROOT", "/");

	define("DB_SERVER", "nehasil2.db.5350222.hostedresource.com");
	define("DB_USER", "nehasil2");
	define("DB_PASSWORD", "NhPark817!");
	define("DB_DATABASE", "nehasil2");
}

define("STATIC_ROOT", URL_ROOT . "templates/" . TEMPLATE_PACKAGE . "/"); // points to the web root of the template
define("IMAGE_ROOT", STATIC_ROOT . "images/");
define("CSS_ROOT", STATIC_ROOT . "css/");

// usage defines
define("EDITABLES_COUNT", 0); // if > 0 render editables in index.php context
define("USE_DROPDOWN", false); // if true render downdown in the context
define("MAIN_PAGE", "page/1/");
define("EVENT_CATEGORIES", "Entertainment,Community,Sports");
define("GENERAL_EVENT_NAME", "General Events");
define("USE_CACHE", false);
define("CACHE_DRIVER_ORDER", "APC,DB");
define("CAN_ADD_PAGES", true);
define("USE_DIRECTORY", false);
define("USE_EVENTS", false);

define("USE_GALLERY", true);
define("GALLERY_NAMES", "Vision,Photos");

// app use constants
require_once("app_defines.php");