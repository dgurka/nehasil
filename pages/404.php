<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

echo $twig->render('404.html', $context);