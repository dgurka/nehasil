CREATE  TABLE `gallery_image` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `gallery` INT NOT NULL ,
  `order` INT NOT NULL ,
  `caption` VARCHAR(255) NULL ,
  `imgloc` VARCHAR(255) NULL ,
  PRIMARY KEY (`ID`) );
